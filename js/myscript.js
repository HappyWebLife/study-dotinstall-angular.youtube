var mainCtrl = function($scope,$http){
	$scope.doSearch = function(){
		var url = 'http://gdata.youtube.com/feeds/api/videos?'
			+ [
				'q='+encodeURIComponent($scope.query),
				'alt=json',
				'v=2',
				'callback=JSON_CALLBACK'
			].join('&');

		$http.jsonp(url).success(function(data){
			console.dir(data);
			$scope.results = data.feed.entry;
		});
	}
}